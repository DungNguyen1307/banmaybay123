using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletEnemy : MonoBehaviour
{
    public float Speed;
    GameObject EnemyBullet;

    // Start is called before the first frame update
    void Start()
    {
        EnemyBullet = gameObject;
        Speed = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        EnemyBullet.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -Speed);
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("Player1"))
        {
            StartCoroutine(DesBullet());
        } else if (collider.CompareTag("Player2"))
        {
            StartCoroutine(DesBullet());
        }
    }


    IEnumerator DesBullet()
    {
        yield return null;
        Destroy(EnemyBullet);
    }
}
