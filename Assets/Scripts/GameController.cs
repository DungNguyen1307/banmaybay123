using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class GameController : MonoBehaviour
{
    public static GameController instance;

    private void Awake()
    {
        MakeInstance();   
    }

    void MakeInstance()
    {
        if (instance == null)
        {
            instance = this;
        }
    }


    [SerializeField]
    private GameObject PanelPause;

    [SerializeField]
    private GameObject PanelDie;
    public void PauseGame()
    {
        PanelPause.SetActive(true);
        Time.timeScale = 0;
    }

    public void ResumeButton()
    {
        PanelPause.SetActive(false);
        Time.timeScale = 1;
    }

    public void RestartButton()
    {
        SceneManager.LoadScene("Gameplay");
        Time.timeScale = 1;
    } 

    public void MenuButton()
    {
        SceneManager.LoadScene("menu");
        Time.timeScale = 1;
    }

    public void DiedPanel()
    {
        PanelDie.SetActive(true);
        Time.timeScale = 0;
    }

}
