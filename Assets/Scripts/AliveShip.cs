using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AliveShip : MonoBehaviour
{
    public float Speed;

    public GameObject obj;

    private float roll;
    private float pitch;

    private float minX, maxX, minY, maxY;

    [SerializeField]
    GameObject Bullet;

    bool canShoot = true;

    [SerializeField]
    GameObject Impact02;

    private float Boom = 2f;

    public GameController gameController;
    // Start is called before the first frame update
    void Start()
    {
        obj = gameObject;
        Speed = 5f;

        Vector3 Bounds = Camera.main.ScreenToWorldPoint(new Vector2(Screen.width, Screen.height));

        minX = -Bounds.x;
        maxX = Bounds.x;

        minY = -Bounds.y;
        maxY = Bounds.y;

        obj.transform.position = new Vector2(-1, -5);
    }

    // Update is called once per frame
    void Update()
    {
        ShipMove();

        ShipBound();

        BulletFire();
    }

    void ShipMove()
    {
        roll = Input.GetAxis("Horizontal") * Speed;
        pitch = Input.GetAxis("Vertical") * Speed;

        obj.GetComponent<Rigidbody2D>().velocity = new Vector2(roll, pitch);
    }

    void ShipBound()
    {
        Vector3 temp = transform.position;

        if(temp.x < minX)
        {
            temp.x = minX;
        } else if(temp.x > maxX)
        {
            temp.x = maxX;
        }

        if (temp.y < minY)
        {
            temp.y = minY;
        } else if(temp.y > maxY)
        {
            temp.y = maxY;
        }

        transform.position = temp;
    }

    void BulletFire()
    {
        if (Input.GetKey(KeyCode.Space))
        {
            if (canShoot)
            {
                StartCoroutine (Shoot());
            }
        }
    }

   IEnumerator Shoot()
    {
        canShoot = false;

        Vector3 temp = transform.position;
        temp.y += 1.2f;

        Instantiate (Bullet, temp, Quaternion.identity);

        yield return new WaitForSeconds(0.4f);

        canShoot = true;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("EnemyShip"))
        {
            StartCoroutine(AliveExplosion());
        }
    }

    IEnumerator AliveExplosion()
    {
        yield return null;
        GameObject AliveEx = Instantiate(Impact02, transform.position, Quaternion.identity) as GameObject;
        Destroy(obj);
        Destroy(AliveEx, Boom);
        GameController.instance.DiedPanel();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.CompareTag("BulletEnemy"))
        {
            StartCoroutine(AliveExplosion());
        } else if(collider.CompareTag("Obstacle")){
            StartCoroutine(AliveExplosion());
        }
    }
}
