using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerRock : MonoBehaviour
{
    [SerializeField]
    GameObject Obstacle;

    BoxCollider2D Box1;
    // Start is called before the first frame update
    private void Awake()
    {
        Box1 = GetComponent<BoxCollider2D>();
    }

    void Start()
    {
        StartCoroutine(SpawnObstacle());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnObstacle()
    {
        yield return new WaitForSeconds(Random.Range(1f, 10f));

        float minX = -Box1.bounds.size.x / 2f;
        float maxX = Box1.bounds.size.x / 2f;

        Vector2 temp = transform.position;
        temp.x = Random.Range(minX, maxX);
        Instantiate(Obstacle, temp, Quaternion.identity);

        StartCoroutine(SpawnObstacle());
    }
}
