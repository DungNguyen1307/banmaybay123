using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawer : MonoBehaviour
{
    [SerializeField]
    private GameObject Enemy;

    private BoxCollider2D box;
    // Start is called before the first frame update
    private void Awake()
    {
        box = GetComponent<BoxCollider2D>();
    }

    void Start()
    {
        StartCoroutine(SpawnerEnemy());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator SpawnerEnemy()
    {
        yield return new WaitForSeconds(Random.Range(1f,8f));

        float minX = -box.bounds.size.x / 2f;
        float maxX = box.bounds.size.x / 2f;

        Vector2 temp = transform.position;
        temp.x = Random.Range(minX, maxX);
        Instantiate(Enemy, temp, Quaternion.identity);

        StartCoroutine(SpawnerEnemy());
    }
}
